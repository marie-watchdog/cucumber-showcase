const common = `
    --require feature/support/hooks.js
    --require step-definitions/**/*.js
`
module.exports = {
    default: `${common} features/**/*.feature`,
}