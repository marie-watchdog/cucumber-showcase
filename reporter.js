const reporter = require('cucumber-html-reporter')

const options = {
    theme: 'bootstrap',
    jsonFile: 'cucumber_report.json',
    output: 'reports/cucumber_report.html',
    reportSuiteAsScenario: true,
    scenarioTimestamp: true,
    launchReport: false,
    metadata: {
        'Test Environment': 'STAGING',
        Browser: 'Webkit 17.4',
        Platform: 'MacOS Sonoma 14.1.1'
    },
}

reporter.generate(options)