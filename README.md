This is a test project to familiarize myself with Cucumber and Gherkin tools.
It contains of a couple of test cases for me to grasp the syntax and available features.

### Requirements for running tests locally:

**node v21.1.0** or higher

To check if you already have it installed run in the terminal:
```
node -v
```
_If you need to install Node.js for your OS, please, visit [Node.js site](https://nodejs.org/)_
___________________

**Install all dependencies by typing in the terminal:**
```
npm install playwright @cucumber/cucumber cucumber-html-reporter

npx playwright install
```
**Then run the tests by:**
```
npm run test
```
**Generate the report by:**
```
npm run report
```
___________________

### Test cases all can be run in the pipeline:
Open Build -> Pipelines and click on the Run Pipeline button in the top right corner.
There are no variables available, so just click Run Pipeline and wait for the results.

