const playwright = require('playwright')
const { Before, After, BeforeAll, AfterAll} = require('@cucumber/cucumber')

BeforeAll( async() => {
    console.log("Launching the Browser...")
    global.browser = await playwright['firefox'].launch({ headless: true })
})

AfterAll( async() => {
    console.log("Closing the Browser...")
    await global.browser.close()
})

Before( async() => {
    console.log("Creating new context and the page...")
    global.context = await global.browser.newContext()
    global.page = await global.context.newPage()
})

After( async() => {
    console.log("Closing the context and the page...")
    await global.page.close()
    await global.context.close()
})