Feature: Login action

  As a user
  I want to login into the application

  Scenario: Login as a standard user with the valid credentials
    Given I visit a login page
    When I fill the login form with a valid credentials
    Then I should see the home page

  Scenario Outline: Login as a standard user with the invalid credentials
    Given I visit a login page
    When I fill the login form with "<username>" and "<password>"
    Then I wait for 1 second

    Examples:
    | username      | password     |
    | standard_user |              |
    |               | secret_sauce |
    |               |              |
    | standard_user | 1#$^*&@<>!(+)|