const { defineStep } = require("@cucumber/cucumber")
const { LoginPage } = require('../../page-objects/login-page')

const loginPage = new LoginPage()
defineStep("I visit a login page", async function()
{
    await loginPage.openLoginPage()
})

defineStep("I fill the login form with a valid credentials", async function() {
    await loginPage.loginToApp('standard_user', 'secret_sauce')
})

defineStep("I should see the home page", async function() {
    await loginPage.assertLogin()
})

defineStep("I wait for 1 second", async function() {
    await loginPage.pause()
})

defineStep(/^I fill the login form with "([^"]*)" and "([^"]*)"$/,
    async function (username, password) {
        await loginPage.loginToApp(username, password)
})