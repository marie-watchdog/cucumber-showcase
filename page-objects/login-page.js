class LoginPage {
    async openLoginPage() {
        await page.goto('https://www.saucedemo.com/')
    }

    async assertLogin() {
        await page.waitForSelector('.inventory_list')
    }

    async pause() {
        await page.waitForTimeout(1000)
    }

    async loginToApp(username, password) {
        await page.getByPlaceholder('Username').fill(username)
        await page.getByPlaceholder('Password').fill(password)
        await page.getByText('Login').click()

    }
}

module.exports = { LoginPage }